import os
from werkzeug.utils import secure_filename
from flask import Flask, render_template, request
from photo_processor import PhotoProcessor


UPLOAD_FOLDER = '/tmp/.incoming'
# UPLOAD_FOLDER = 'tmp'

ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif'}


app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
photo_processor = PhotoProcessor()


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


@app.route('/')
def form():
    return render_template('index.html')


@app.route('/content/upload', methods=['GET', 'POST'])
def upload_file():
    status = ''

    if request.method == 'POST':
        # check if the post request has the file part
        if 'img_file' not in request.files:
            status = 'No file part.'
        else:
            p_file = request.files['img_file']
            # if user does not select file, browser also submit a empty part without filename.
            if p_file.filename == '':
                status = 'No selected file.'
            elif p_file and allowed_file(p_file.filename):
                if request.form['filename'] == '':
                    filename = secure_filename(p_file.filename)
                else:
                    filename = request.form['filename']
                    if filename[-4:] not in ALLOWED_EXTENSIONS:     # add extension
                        filename += '.jpg'

                file_path = app.config['UPLOAD_FOLDER'] + '/' + filename
                p_file.save(file_path)

                photo_processor.set_img_file(file_path)
                photo_processor.start_process()

                status = 'Success.'
            else:
                status = 'Unsupported file type.'

    return render_template('form_action.html', status=status)


if __name__ == '__main__':

    if not os.path.exists(UPLOAD_FOLDER):
        os.makedirs(UPLOAD_FOLDER)
    photo_processor.start()

    app.run(
        host="0.0.0.0",
        port=int("8080")
    )
